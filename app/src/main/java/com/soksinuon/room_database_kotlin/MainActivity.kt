package com.soksinuon.room_database_kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var deletedTransaction: Transaction
    private lateinit var transactions: List<Transaction>
    private lateinit var oldtransactions: List<Transaction>
    private lateinit var transactionAdapter: TransactionAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var db : AppDatabase
    //private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val addBtn       = findViewById<FloatingActionButton>(R.id.addBtn)


        transactions = arrayListOf()

        /*For Room database init and create room database */
        db = Room.databaseBuilder(this,
        AppDatabase::class.java,
        "transaction").build()

        /* For inti new object */
        transactionAdapter  = TransactionAdapter(transactions)
        linearLayoutManager = LinearLayoutManager(this)


        recyclerView.apply {
            adapter = transactionAdapter
            layoutManager = linearLayoutManager

        }
        /* Swipe to remove */
        val itemTouchHelper = object : ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.RIGHT){
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
               return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                deleteTransaction(transactions[viewHolder.adapterPosition])

            }
        }
        val swipeHelper = ItemTouchHelper(itemTouchHelper)
        swipeHelper.attachToRecyclerView(recyclerView)


        /* For Btn Click to Screen AddTransaction */
        addBtn.setOnClickListener {
            val intent = Intent(this, AddTransactionActivity::class.java)
            startActivity(intent)
        }
    }
    private fun fetchAll() {

        GlobalScope.launch {

            transactions = db.transactionDao().getAll()

            runOnUiThread {
                updateDashboard()
                transactionAdapter.setData(transactions)

            }
        }
    }

    private fun showSnackbar(){
        val view = findViewById<View>(R.id.coordinator)
        val snackbar = Snackbar.make(view, "Transaction deleted ! ", Snackbar.LENGTH_LONG)
        snackbar.setAction("Undo"){
            undoDelete()
        }
            .setActionTextColor(ContextCompat.getColor(this, R.color.red))
            .setTextColor(ContextCompat.getColor(this,R.color.white))
            .show()
    }

    /* For Delete*/
    private fun deleteTransaction(transaction: Transaction){
        deletedTransaction  = transaction
        oldtransactions     = transactions

        GlobalScope.launch {
            db.transactionDao().delete(transaction)

            transactions = transactions.filter { it.id != transaction.id }
            runOnUiThread {
                updateDashboard()
                transactionAdapter.setData(transactions)
                showSnackbar()
            }
        }
    }
    private fun updateDashboard(){

        /*For FindViewById */
        val balance      = findViewById<TextView>(R.id.balance)
        val budget       = findViewById<TextView>(R.id.budget)
        val expense      = findViewById<TextView>(R.id.expense)

         val totalAmount  = transactions.map { it.amount }.sum()
         val budgetAmount = transactions.filter { it.amount>0 }.map { it.amount }.sum()
         val expenseAmount  = totalAmount - budgetAmount

          balance.text = "$ %.2f".format(totalAmount)
          budget.text = "$ %.2f".format(budgetAmount)
          expense.text="$ %.2f".format(expenseAmount)

    }

    /* For we want Delete or Don't Want Delete */
    private fun undoDelete(){
        GlobalScope.launch {
            db.transactionDao().insertAll(deletedTransaction)
            transactions = oldtransactions

            runOnUiThread {
                transactionAdapter.setData(transactions)
                updateDashboard()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        fetchAll()
    }
}