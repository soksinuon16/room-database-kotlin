package com.soksinuon.room_database_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import androidx.core.widget.addTextChangedListener
import androidx.room.Room
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DetailedActivity : AppCompatActivity() {

     /*declare form room database class for working update and detail screen */
    private lateinit var transaction: Transaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed)

        val updateBtn         = findViewById<Button>(R.id.updateBtn)
        val labelInout        = findViewById<TextInputEditText>(R.id.labelInput)
        val amountInput       = findViewById<TextInputEditText>(R.id.amountInput)
        val labelLayout       = findViewById<TextInputLayout>(R.id.labelLayout)
        val amountLayout      = findViewById<TextInputLayout>(R.id.amountLayout)
        val closeBtn          = findViewById<ImageButton>(R.id.closeBtn)
        val descriptionInput  = findViewById<TextInputEditText>(R.id.descriptionInput)

        /* this place for get intent for */
        transaction = intent.getSerializableExtra("transaction") as Transaction

        labelInout.setText(transaction.label)
        amountInput.setText(transaction.amount.toString())
        descriptionInput.setText(transaction.description)


        labelInout.addTextChangedListener {
            updateBtn.visibility = View.VISIBLE
            if (it!!.count()>0){
                labelLayout.error = null
            }
        }

        amountInput.addTextChangedListener {
            updateBtn.visibility = View.VISIBLE
            if (it!!.count()>0)
                amountLayout.error = null

        }

        descriptionInput.addTextChangedListener {
            updateBtn.visibility = View.VISIBLE
            if (it!!.count()>0)
                amountLayout.error = null
        }

        updateBtn.setOnClickListener {
            val label = labelInout.text.toString()
            val description = descriptionInput.text.toString()
            val amount = amountInput.text.toString().toDoubleOrNull()

            if (label.isEmpty())
                labelLayout.error = "Please neter a valid label"

            else if (amount == null)
                amountLayout.error  = "Please enter a valid amount"

            else{
                val transaction = Transaction( transaction.id, label, amount, description)
                update(transaction)
            }
        }

        closeBtn.setOnClickListener {
            finish()

        }
    }
    private fun  update(transaction: Transaction){
        val db = Room.databaseBuilder(this,
            AppDatabase::class.java,
            "transaction").build()

        GlobalScope.launch {
            db.transactionDao().insertAll(transaction)
            finish()
        }
    }

}