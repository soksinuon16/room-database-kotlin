package com.soksinuon.room_database_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import androidx.core.widget.addTextChangedListener
import androidx.room.Room
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AddTransactionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_transaction)

        val addTransactionBtn = findViewById<Button>(R.id.addTransactionBtn)
        val labelInout = findViewById<TextInputEditText>(R.id.labelInput)
        val amountInput = findViewById<TextInputEditText>(R.id.amountInput)
        val labelLayout = findViewById<TextInputLayout>(R.id.labelLayout)
        val amountLayout = findViewById<TextInputLayout>(R.id.amountLayout)
        val closeBtn     = findViewById<ImageButton>(R.id.closeBtn)
        val descriptionInput  = findViewById<TextInputEditText>(R.id.descriptionInput)

        labelInout.addTextChangedListener {
            if (it!!.count()>0){
                labelLayout.error = null
            }
        }
        addTransactionBtn.setOnClickListener {
            val label = labelInout.text.toString()
            val description = descriptionInput.text.toString()
            val amount = amountInput.text.toString().toDoubleOrNull()

         if (label.isEmpty())
             labelLayout.error = "Please neter a valid label"

         else if (amount == null)
             amountLayout.error  = "Please enter a valid amount"

         else{
             val transaction = Transaction(0, label, amount, description)
             insert(transaction)
         }

        }

        closeBtn.setOnClickListener {
            finish()

        }

    }
    private fun insert(transaction: Transaction){
        val db = Room.databaseBuilder(this,
            AppDatabase::class.java,
            "transaction").build()

        GlobalScope.launch {
            db.transactionDao().insertAll(transaction)
            finish()
        }
    }

}